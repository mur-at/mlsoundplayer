#!/usr/bin/env python

import sys
import os
import librosa
import librosa.display
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

def usage():
    print('\n\nusage:  {} <output_directory> <input_filename>\n\n'.format(sys.argv[0]))

if __name__ == '__main__':
    tmpfilename = '/tmp/tmpspeck.png'

    if len(sys.argv) != 3:
        sys.exit(1)

    outdir = sys.argv[1]
    if not os.path.isdir(outdir):
        usage()
        print('directory {} not found'.format(outdir))
        sys.exit(1)

    infilename = sys.argv[2]
    if not os.path.isfile(infilename):
        usage()
        print('file {} not found'.format(infilename))
        sys.exit(1)

    basename = os.path.basename(infilename)[:-4]
    outfilename = os.path.join(outdir, '{}.png'.format(basename))

    wf, sr = librosa.load(infilename)
    plt.figure(figsize=(8, 3))
    D = librosa.amplitude_to_db(np.abs(librosa.stft(wf)), ref=np.max)
    librosa.display.specshow(D, y_axis='linear')

    # plt.savefig(os.path.join('cover', '{}.png'.format(basename)))
    plt.savefig(tmpfilename)
    img = Image.open(tmpfilename)
    cropped = img.crop((100, 36, 720, 267))
    cropped.save(outfilename)
    print('Saved as {}.png'.format(basename))

