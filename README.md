# ML-Soundplayer

A webpage that displays sounds in directory sound/ and plays them using
[amplitudejs](https://521dimensions.com/open-source/amplitudejs).

## Prerequisites

* python librosa
* python matplotlib
* python PIL

## Usage

* Place a number of soundfiles in .wav format in the directory `input/`
* run `processinput`
* open `index.html` in your favorite (modern!) browser
